vasemode_extrusion(25.4,0.2); // total height, layer height

module vasemode_extrusion(h,lh)
{
    intersection()
    {
        union()
        {   // rotate the gap every layer
            for (z=[0:4*lh:h])
            {
                translate([0,0,z])
                linear_extrude(lh, convexity=10)
                    extrusion_shape();
                translate([20,0,lh+z])
                rotate([0,0,90])
                linear_extrude(lh, convexity=10)
                    extrusion_shape();
                translate([20,20,2*lh+z])
                rotate([0,0,180])
                linear_extrude(lh, convexity=10)
                    extrusion_shape();
                translate([0,20,3*lh+z])
                rotate([0,0,270])
                linear_extrude(lh, convexity=10)
                    extrusion_shape();
            }
        }
        cube([20,20,h]); // trim to desired height
    }
}

module extrusion_shape()
{
    difference()
    {
        translate([-25,30])
            import("20mm-M5-W1pt6-X1.dxf"); // from https://www.thingiverse.com/thing:10261
        translate([10,10])
            circle(d=6, $fn=60);
        translate([10-0.1/2,12])
            square([0.1,2.5]);
    }
}
